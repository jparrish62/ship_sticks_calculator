FactoryBot.define do
  factory :product do
    name {"String"}
    length {1}
    width {12}
    height {5}
    weight {4}
  end
end
