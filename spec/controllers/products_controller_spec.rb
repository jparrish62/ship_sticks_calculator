require "rails_helper"

RSpec.describe ProductsController, type: :controller do
  describe 'GET #index' do
    it 'returns a successful response' do
      get :index
      expect(response).to be_successful
    end

    it 'assigns all products to @products' do
      product = FactoryBot.create(:product)
      get :index
      expect(Product.find(product.id).name).to eq(product.name)
    end
  end

  describe 'GET #show' do
    let(:product) {FactoryBot.create(:product)}

    it 'return a successful reponse'do
      get :show, params: {id: product.id} 
      expect(response).to be_successful
    end

    it 'assigns the requested product to @product' do
      get :show, params: {id: product.id}, format: :json
      expect(assigns(:product)).to eq(product)
    end
  end

  describe 'POST #create' do
    let(:valid_attributes) {FactoryBot.build(:product).attributes}
    let(:invalid_attributes) {FactoryBot.build(:product, name: nil).attributes}

    context "with valid parameters" do
      it "creates a new product" do
        expect do
          post :create, params: { product: valid_attributes }
        end.to change(Product, :count).by(1)
      end

      it 'returns a created response' do
        post :create, params: {product: valid_attributes}
        expect(response).to have_http_status(:created)
      end
    end

    context "with invalid parameters" do
      it "does not create a new product" do
        expect do
          post :create, params: {product: invalid_attributes}
        end.to change(Product, :count).by(0)
      end

      it 'returns response of unprocessable entity' do
        post :create, params: {product: invalid_attributes}
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    describe "PUT #update" do
      let!(:product) {FactoryBot.create(:product)}
      context "when attributes are valid" do
        before {
          put :update, params: {id: product.id, product: {name: 'updated product' }}
        }
        it "updates the requested product" do
          p = Product.find(product.id)
          p.reload
          expect(p.name).to eq('updated product')
        end

        it "updates the requested product" do
          put :update, params: {id: product.id, product: {name: 'updated product' }}
          product.reload
          expect(response).to be_successful
        end
      end

      context "with invalid parameter" do 
        it "does not update the requested product" do
          put :update, params: {id: product.id, product: {name: nil }}
          expect(response.body.include?("can't be blank")).to eq true
        end

        it "returns status 422" do
          put :update, params: {id: product.id, product: {name: nil }}
          expect(response.status).to eq 422
        end
      end
    end

    describe 'DELETE #destroy' do
      let!(:product) { FactoryBot.create(:product) }

      it 'destroys the requested product' do
        expect do
          delete :destroy, params: { id: product.id }
        end.to change(Product, :count).by(-1)
      end
    end

    describe 'POST #find_products' do
      let!(:product1) { FactoryBot.create(:product, name: "Golf Bag", length: 12)}
      let!(:product2) { FactoryBot.create(:product, name: "Ski Bag", length: 41, width: 11, height: 24, weight: 72)}
      let!(:product3) { FactoryBot.create(:product, name: "Large Bag", length: 25, width: 15, height: 7, weight: 25)}
      let!(:product4) { FactoryBot.create(:product, name: "Small Bag")}
      let!(:product5) { FactoryBot.create(:product, name: "Largest Golf Bag", length: 50, width: 50, height: 50, weight: 85)}

      context 'when params are passed in that match a specific product' do
        before do
          attributes = {length: 25, width: 15, height: 7, weight: 25}
          post :find_products, params: {product: attributes}
        end

        it 'returns the corresponding product' do
          expect(response.body.include?(product3.name)).to eq true
          expect(response.status).to eq 200
        end
      end

      context 'when params are passed in that do not match a specific product(small params)' do
        before do
          attributes = {length: 22, width: 22, height: 10, weight: 20}
          post :find_products, params: {product: attributes}
        end

        it 'return the next largest product' do
          expect(response.body.include?(product2.name)).to eq true
          expect(response.status).to eq 200
        end
      end

      context 'when params are passed in that do not match any products(large params)' do
        before do
          attributes = {length: 99, width: 99, height: 99, weight: 99}
          post :find_products, params: {product: attributes}
        end

        it 'returns largest product' do
          expect(response.body.include?(product5.name)).to eq true
          expect(response.status).to eq 200
        end
      end
    end
  end
end











