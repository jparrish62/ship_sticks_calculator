require 'rails_helper'

RSpec.describe Query::ProductService do
  describe '.call' do
    let(:params) do
      {
        product: {
          length: 10,
          width: 5,
          height: 3,
          weight: 15
        }
      }
    end

    it 'calls the instance method' do
      expect_any_instance_of(described_class).to receive(:call)
      described_class.call(params)
    end
  end

  describe '#call' do
    let(:params) do
      {
        product: {
          length: 10,
          width: 5,
          height: 3,
          weight: 15
        }
      }
    end

    subject { described_class.new(params).call }

    it 'calls find_matching_products' do
      expect_any_instance_of(described_class).to receive(:find_matching_products)
      subject
    end
  end

  describe '#find_matching_products' do
    context 'when product is present' do
      let(:product_service) { described_class.new(product: { length: 10, width: 5, height: 3, weight: 15 }) }

      before do
        allow(product_service).to receive(:product).and_return(double('Product'))
      end

      it 'returns the product' do
        expect(product_service.find_matching_products).to eq(product_service.product)
      end
    end

    context 'when product is nil' do
      let(:product_service) { described_class.new(product: { length: 10, width: 5, height: 3, weight: 15 }) }

      before do
        allow(product_service).to receive(:product).and_return(nil)
        allow(product_service).to receive(:next_largest_product).and_return(double('NextLargestProduct'))
      end

      it 'returns the next largest product' do
        expect(product_service.find_matching_products).to eq(product_service.next_largest_product)
      end
    end
  end
end
