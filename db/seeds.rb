require 'json'

file = File.read('./products.json')
data = JSON.parse(file)

data["products"].each do |product_data|
  Product.create!(
    name: product_data["name"],
    type: product_data["type"],
    length: product_data["length"],
    width: product_data["width"],
    height: product_data["height"],
    weight: product_data["weight"]
  )
end
