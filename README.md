Project README
Getting Started
This README provides instructions to set up and run the application.

Ruby Version
Ensure you have Ruby version 2.7.2 installed.

Install JavaScript dependencies using Yarn:
yarn install

System Dependencies
Run the following command to install the required gems using Bundler:
bundle install

Database Initialization
Pull the MongoDB Docker image:
docker pull mongo

Run the MongoDB container, mapping port 27017:
docker run -d -p 27017:27017 <container_id>

List all Docker containers to get the container ID:
docker ps -a

Initialize the database and seed data using Rails:
bundle exec rails db:seed

Running the Test Suite
Execute the following command to run the test suite with RSpec:
bundle exec rspec

Running the Application
To start the application, run the following command:
bin/dev

This should start the application and make it accessible locally.
