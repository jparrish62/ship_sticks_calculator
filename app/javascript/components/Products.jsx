import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Table } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function Products() {
  const [showModal, setShowModal] = useState(false);
  const [dimensions, setDimensions] = useState({ length: '', width: '', height: '' });
  const [weight, setWeight] = useState('');
  const [productName, setProductName] = useState('');
  const [upSaleProductName, setUpSaleProductName] = useState('');
  const [products, setProducts] = useState([]);
  const [productId, setProductId] = useState([]);
  const [editingProduct, setEditingProduct] = useState(null);
  const [createProduct, setCreateProduct] = useState(null);
  const [showSizeWarningModal, setShowSizeWarningModal] = useState(false);
  const [errors, setErrors] = useState({});

  useEffect(() => {

    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await fetch('http://localhost:3000/products');
      const productsData = await response.json();
      setProducts(productsData);
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };

  const validateAndSetField = (fieldName, value, validationFunction) => {
    const errorMessage = validationFunction(fieldName, value);
    setErrors({ ...errors, [fieldName]: errorMessage });

    if (fieldName === 'name') {
      setProductName(value);
    } else {
      setDimensions({ ...dimensions, [fieldName]: value });
      if (fieldName === 'weight') {
        setWeight(value);
      }
    }
  };

  const validateName = (fieldName, value) => {
    return value.trim() ? null : 'Name must not be empty';
  };

  const validateDimension = (fieldName, value) => {
    return value.trim() ? null : `${fieldName.charAt(0).toUpperCase() + fieldName.slice(1)} must not be empty`;
  };

  const validateWeight = (fieldName, value) => {
    return value.trim() ? null : 'Weight must not be empty';
  };

  const handleLaunchCalculator = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setEditingProduct(null);
    setProductName('');
    setDimensions('');
    setWeight('');
    setCreateProduct('');
    setShowModal(false);
  };

  const handleCalculate = async () => {
    try {
      if (editingProduct) {
        const response = await fetch(`http://localhost:3000/products/${editingProduct._id['$oid']}`, {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ product: { ...dimensions, weight } }),
        });

        if (response.ok) {
          const editResponse = await response.json()
          console.log('Product updated successfully', editResponse);
        } else {
          console.error('Error updating product:', response.statusText);
        }
      } else if (createProduct) {
        const response = await fetch('http://localhost:3000/products', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ product: { name: productName, ...dimensions, weight } }),
        });

        if (response.ok) {
          console.log('Product created successfully');
          fetchProducts();
        } else {
          console.error('Error creating product:', response.statusText);
        }
      } else {
        const response = await fetch('http://localhost:3000/find_products', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ product: { ...dimensions, weight } }),
        });

        if (response.ok) {
          const upSaleProduct = await response.json();
          setUpSaleProductName(upSaleProduct["name"]);
          console.log('Product created successfully', upSaleProductName);
        } else {
          console.error('Error creating product:', response.statusText);
        }
      }
    } catch (error) {
      console.error('Error:', error);
    }

    setTimeout(() => {
      handleCloseModal();
    }, 1000);
  };

  const handleCreateProduct = (product) => {
    setCreateProduct("Create")
    setShowModal(true);
  }

  const handleEditProduct = (product) => {
    setEditingProduct(product);
    setProductName(product.name);
    setDimensions({length: product.length, width: product.width, height: product.height });
    setWeight(product.weight);
    setProductId(product._id['$oid']);
    setShowModal(true);
  };

  const handleDeleteProduct = async (productId) => {
    try {
      const csrfToken = document.querySelector("meta[name=csrf-token]").content;

      const response = await fetch(`http://localhost:3000/products/${productId}`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'X-CSRF-Token': csrfToken,
        },
      });

      console.log("Deleting")
      if (response.ok) {
        console.log('Product deleted successfully');
        fetchProducts(); 
      } else {
        console.error('Error deleting product:', response.statusText);
      }
    } catch (error) {
      console.error('Error deleting product:', error);
    }
  };
  return (
    <div className="App">
      <h1>Shipping Calculator</h1>
      <Button variant="success" className="create-button" onClick={() => handleCreateProduct()}>
        Create Product
      </Button>
       &nbsp; 
      <Button variant="success" onClick={handleLaunchCalculator}>Launch Calculator</Button>
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Shipping Calculator</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              {createProduct && (
                <>
                  &nbsp;
                  <Form.Label>Name</Form.Label>
                  <Form.Control
                    id="Name"
                    type="text"
                    placeholder="Name"
                    value={productName}
                    onChange={(e) => validateAndSetField('name', e.target.value, validateName)}
                  />
                  {errors.name && <Form.Text className="text-danger">{errors.name}</Form.Text>}
                </>
              )}
               &nbsp;
              <Form.Control
                id="Length"
                type="text"
                placeholder="Length"
                value={dimensions.length}
                onChange={(e) => validateAndSetField('length', e.target.value, validateDimension)}
              />
              {errors.length && <Form.Text className="text-danger">{errors.length}</Form.Text>}
              &nbsp; 
              <Form.Control
                id="Width"
                type="text"
                placeholder="Width"
                value={dimensions.width}
                onChange={(e) => validateAndSetField('width', e.target.value, validateDimension)}
              />
              {errors.width && <Form.Text className="text-danger">{errors.width}</Form.Text>}
              &nbsp; 
              <Form.Control
                id="Height"
                type="text"
                placeholder="Height"
                value={dimensions.height}
                onChange={(e) => validateAndSetField('height', e.target.value, validateDimension)}
              />
              {errors.height && <Form.Text className="text-danger">{errors.height}</Form.Text>}
            </Form.Group>
            &nbsp; 
            <Form.Group>
              <Form.Control
                id="Weight"
                type="text"
                placeholder="Weight"
                value={weight}
                onChange={(e) => validateAndSetField('weight', e.target.value, validateWeight)} 
              />
              {errors.weight && <Form.Text className="text-danger">{errors.weight}</Form.Text>}
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
            <Button variant="success" onClick={handleCalculate}>
              {editingProduct
                ? 'Update Product'
                : createProduct
                ? 'Create Product'
                : 'Find Product'}
            </Button>
        </Modal.Footer>
      </Modal>

      {upSaleProductName && (
        <div className="result">
          <p>We think the "{upSaleProductName}" is the perfect bag for you </p>
        </div>
      )}

      <div className="product-list">
        <h2>Product List</h2> 
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Name</th>
              <th>Dimensions</th>
              <th>Weight</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {products.map((product) => (
              <tr key={product._id['$oid']}>
                <td>{product.name}</td>
                <td>{`${product.length} x ${product.width} x ${product.height}`}</td>
                <td>{product.weight}</td>
                <td>
                  <Button className="btn-dark" variant="success" onClick={() => handleEditProduct(product)}>
                    Edit
                  </Button>
                   &nbsp; 
                  <Button variant="success" onClick={() => handleDeleteProduct(product._id['$oid'])}>
                    Delete
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
}

export default Products;
