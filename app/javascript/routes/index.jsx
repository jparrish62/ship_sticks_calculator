import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Products from "../components/Products";

export default (
  <Router>
    <Routes>
      <Route path="/" element={<Products />} />
    </Routes>
  </Router>
);
