module Query
  class ProductService
    class << self
      def call(params)
        new(params).call
      end
    end

    attr_reader :length, :width, :height, :weight

    def initialize(params)
      @length = params[:product][:length]
      @width = params[:product][:width]
      @height = params[:product][:height]
      @weight = params[:product][:weight]
    end

    def call
      find_matching_products
    end

    def find_matching_products
      return product if product.present?
      return largest_product if next_largest_product == nil

      next_largest_product
    end

    def product
      @product ||= Product.where(length: length)
                          .where(width: width)
                          .where(height: height)
                          .where(weight: weight)
                          .first
    end

    def next_largest_product
      @next_largest ||= Product.where(query).order_by(length: 1, width: 1, height: 1, weight: 1).first
    end

    def largest_product
      @largest_product ||= Product.all.max_by(&:weight)
    end

    def query
      {
        "$and" => [
          { "length" => { "$gte" => length } },
          { "width" => { "$gte" => width } },
          { "height" => { "$gte" => height } },
          { "weight" => { "$gte" => weight } }
        ]
      }
    end
  end
end
