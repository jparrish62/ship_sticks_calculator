class ProductsController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :set_product, only: [:show, :update, :destroy]

  def home
  end

  def index
    @products = Product.all
    render json: @products
  end

  def show
    render json:@products
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      render json: @product, status: :created
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  def update 
    if @product.update(product_params)
      render json: @product
    else
      render json: @product.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @product.destroy
  end

  def find_products
    @product = Query::ProductService.call(params)
    render json: @product
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(:name, :length, :width, :height, :width, :weight)
  end
end
