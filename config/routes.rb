Rails.application.routes.draw do
  root 'products#home'
  resources :products, only: [:index, :show, :create, :update, :destroy]
  post 'find_products' => 'products#find_products'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
